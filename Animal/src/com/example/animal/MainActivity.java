package com.example.animal;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		Button btnDog = (Button) findViewById(R.id.btn_dog);
		Button btnCat = (Button) findViewById(R.id.btn_cat);
		Button btnRabbit = (Button) findViewById(R.id.btn_rabbit);

		final ImageView imgDog = (ImageView) findViewById(R.id.img_dog);
		final ImageView imgCat = (ImageView) findViewById(R.id.img_cat);
		final ImageView imgRabbit = (ImageView) findViewById(R.id.img_rabbit);

		btnDog.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				imgDog.setVisibility(View.VISIBLE);
				imgCat.setVisibility(View.GONE);
				imgRabbit.setVisibility(View.GONE);

			}
		});

		btnCat.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				imgDog.setVisibility(View.GONE);
				imgCat.setVisibility(View.VISIBLE);
				imgRabbit.setVisibility(View.GONE);

			}
		});

		btnRabbit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				imgDog.setVisibility(View.GONE);
				imgCat.setVisibility(View.GONE);
				imgRabbit.setVisibility(View.VISIBLE);

			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
