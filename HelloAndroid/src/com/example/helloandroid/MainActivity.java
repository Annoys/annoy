package com.example.helloandroid;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		final ImageView t1 = (ImageView) findViewById(R.id.textView1);
		t1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				// float size = t1.getTextSize() * 2;
				// t1.setTextSize(size);
				// TODO Auto-generated method stub
				// 2. 전화걸기 구글 소스 검색

				Intent intent = new Intent(Intent.ACTION_CALL, Uri
						.parse("tel:010-1111-2222"));
				startActivity(intent);
			}
		})

		;

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
