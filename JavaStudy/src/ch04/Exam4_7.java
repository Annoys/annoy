package ch04;

public class Exam4_7 {

	public static void main(String[] args) {

		int size = 5;

		int center = size / 2 + 1; // 여백 3
		int left = size / 2 + 1; // 왼쪽여백 3
		int right = size / 2 + 1; // 오른쪽여백 3

		for (int i = 1; i <= size; i++) {
			for (int j = 1; j <= size; j++) {

				if (left <= j && j <= right) {

					System.out.print("*"); // left 2 right 4
				} else {
					System.out.print(" ");
				}
			

				// left right 여백 비교후 * 공백출력

			} 
			if ( i >= center) {
				left++;
				right--;
			} else {
			left--;
			right++;
			} System.out.println(" "  + left + " " + right);
			System.out.println();

			// // i의값이 중심점 3을 지난경우
			// left++;
			// right--;
			// // i의값이 중심점 3을 지나지 않는 경우
			// left--;
			// right++
		}

	} 
}
