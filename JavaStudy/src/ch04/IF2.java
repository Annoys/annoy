package ch04;

import java.util.Scanner;

public class IF2 {
	public static void main(String[] args) {

		int score = 0;
		System.out.print("점수를 입력해주세요.(0~100 만 입력) >");

		// 키보드를 이용하여 값 입력 - 라이브러리 = API..?
		Scanner scan = new Scanner(System.in);            //스캔
		score = scan.nextInt();

		if (score >= 0 && score <= 100) {

			if (score >= 90) {                            //중첩

				if (score >= 95) {                        //중첩
					System.out.println("A+");
				} else {
					System.out.println("A");
				}

			} else if (score >= 80) {
				System.out.println("B");
			} else if (score >= 70) {
				System.out.println("C");
			} else if (score >= 60) {
				System.out.println("D");
			} else {
				System.out.println("F");
			}
		} else {
			System.out.println("잘못된 입력값 입니다.");
		}

	}
}
