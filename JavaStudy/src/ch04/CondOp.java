package ch04;

public class CondOp {

	public static void main(String[] args) {
		//(조건식) ? 참 : 거짓 -> 삼항연산자
		int num = 5;
		if(num < 5) {
			System.out.println("탈락");
		} else {
			System.out.println("합격");
		}
		
		// ( num < 5 )? System.out.println("탈락") : System.out.println("합격");
		//                  이걸로... 변경
		String result = (num < 5) ? "탈락" : "합격";
	
	
		
	}
	

}
