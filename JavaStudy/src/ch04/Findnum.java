package ch04;

import java.util.Random;
import java.util.Scanner;

public class Findnum {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Random random = new Random();
		int num = random.nextInt(100) + 1;

		// 무한반복
		while (true) {
			System.out.println("수를 입력해주세요 : ");
			Scanner scan = new Scanner(System.in);
			int input = scan.nextInt();

			if (num < input) {
				System.out.println("down");
			} else {
				if (input == num) {
					System.out.println("정답");
					break;
			} else { 
				System.out.println("up");
			}
		} 
	}     
	}
}
