package ch04;

public class IF1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		final char MALE = 'M';
		final char FEMALE = 'F';

		char gender = 'F';
		if (gender == FEMALE) {
			System.out.println("여자");
		}
		if (gender == MALE) {
			System.out.println("남자");
		}
		// 숫자이면
		boolean isNumeric = true;
		if (isNumeric) { // if(isNumeric == true)
			System.out.println("숫자");
		}
		// 숫자가 아니면
		if (!isNumeric) { // if(isNumeric == false)
			System.out.println("숫자아님");
		}

	}

}
