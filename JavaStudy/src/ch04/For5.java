package ch04;

import java.util.Random;

public class For5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// 랜덤 방법 1. Random 자료형
		Random random1 = new Random();
		// 1 ~ 45 까지나옴
		int num1 = random1.nextInt(45) + 1;
		System.out.println(num1);

		// 랜덤방법 2.
		// 0.0 ~ 0.999999999999999999999999999
		// 0 ~ 10 사이의 랜덤 수 구하기
		// 0.1 * 10 = 1 0.7 * 10 = 7
		// 0.9999999 * 10 = 9
		int num2 = (int) (Math.random() * 45) + 1; // 이거 더블형임...

		System.out.println(num2);

	}

}
