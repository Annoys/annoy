package ch04;

public class For10 {
	public static void main(String[] args) {

		for (int i = 2; i <= 5; i++) {
			for (int j = 1; j <= 9; j++) {
				if ( j == i ) {
					continue;
				}
				System.out.println(i + " x " + j + " = " + (i * j));
				
			}
		}
	}

}
