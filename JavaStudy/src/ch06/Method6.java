package ch06;

public class Method6 {

	int a;                                  // 전역변수 - 메모리상에 등록되지 않음	
	static String b; 						// 전역변수 = 메모리상에 등록됨

	public void method () {
		int aa = a * a;						// 등록되지 않은 변수 사용 가능
		String bb = b + b;					// 등록된 변수도 사용 가능
	}
	public static void method2() {}
	
	public static void main(String[] args) {

		Method6 m6 = new Method6(); 		// a변수를 쓰기 위해서는 메모리 등록 - new 키워드사용
		int aaa = m6.a * m6.a;				// 사용할때는 반드시 변수형.요소
		
		method2();
		
		
	}
}









