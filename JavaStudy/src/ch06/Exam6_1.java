package ch06;

public class Exam6_1 {

	public static void main(String[] args) {
		cal(5, 3);

	}

	public static void cal(int num1, int num2) {
		int a = num1 + num2;
		int b = num1 - num2;
		int c = num1 * num2;
		int e = num1 % num2;
		int d = num1 / num2;

		System.out.println("덧셈 : " + a);
		System.out.println("뺄셈 : " + b);
		System.out.println("곱셈 : " + c);
		System.out.println("나눗셈 : " + d);
		System.out.println("나머지 : " + e);

	}
}
