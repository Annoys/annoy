package ch06;

import org.omg.Messaging.SyncScopeHelper;

public class Exam6_5 {
	static int count = 0;

	public static void main(String[] args) {

		dice();
	}

	public static void dice() {
		int count = 0;
 		int num1 = (int) (Math.random() * 6 + 1);
		int num2 = (int) (Math.random() * 6 + 1);
		count++;
		System.out.println(num1 + " " + num2);

		if (num1 != num2) {

			dice();

			return;

		} else {

			System.out.println("동일한 숫자가 나와서 종료");
			System.out.println("던진횟수 : " + count);
		}
	}

}
