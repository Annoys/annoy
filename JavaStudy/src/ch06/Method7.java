package ch06;

public class Method7 {

	public static void main(String[] args) {

		int score1 = 80;
		int score2 = 80;
		
		System.out.println(score1 == score2);
		
		Method6 m1 = new Method6 ();
		Method6 m2 = new Method6 ();
		
		System.out.println(m1 == m2);
		//생긴 모양은 같지만 같지 않다. (주소값이 다르다.)
/////////////////////////////////////////////////////////////////////
		
		int age = 10;
		int [] arr = {10};		
		
		change(age);   // 기본자료형
		change(arr);   // 참조자료형
		
	
		System.out.println(age);
		System.out.println(arr[0]);
		
		
	}

	public static void change(int age) {
		age *= 2;
		
		
	}

	public static void change(int[] arr) {
		arr[0] *= 2;
	}

	
	
	
	
	
	
	
	
	
	
	
}
