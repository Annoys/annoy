package ch06;

public class Method5 {

	public static void main(String[] args) {
		String result = printStar(10, "*");
		System.out.println(result);
	}
	/** 
	 * 
	 * @param count 
	 * @param special
	 * @return
	 */
	public static String printStar(int count, String special) {
		String sum = "";
		for (int i = 0; i < count; i++) {
			for (int j = 0; j <= i; j++) {
				sum += special;
				// System.out.print(special);
			}
			sum += "\n";
			// System.out.println();
		}
		return sum;
	}
}
