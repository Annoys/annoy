package ch06;

public class Method1 {
	public static void main(String[] args) {
		System.out.println("프로그램 시작");

		// docopy(); // 메소드출력

		doCopy2();
		String result = doCopy2();
		System.out.println("프로그램 끝");
	}

	// return x parameter x
	public static void docopy() {
		System.out.println("복사 시작");
		System.out.println("복사 끝");

	}

	// return o parameter x
	public static String doCopy2() {
		System.out.println("복사 시작");
		System.out.println("복사 끝");
		return "복사 완료";

	}

}
