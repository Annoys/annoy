package ch06;

public class Method3 {

	public static void main(String[] args) {

		// 배열을 만들어놓고;;

		int[] arr1 = { 1, 2, 3, 4, 5, 6 };

		int result1 = sumArray(arr1);

		System.out.println("1 : " + result1);

		// 바로 배열 만드는것

		int result2 = sumArray(new int[] { 5, 6, 7, 8, 9 });

		System.out.println("2 : " + result2);

	}

	public static int sumArray(int[] arr) {
		int sum = 0;
		for (int i = 0; i < arr.length; i++) {
			sum += arr[i];
		}
		return sum;
	}
}
