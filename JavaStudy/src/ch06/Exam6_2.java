package ch06;

import org.omg.Messaging.SyncScopeHelper;

public class Exam6_2 {

	public static void main(String[] args) {
		int n = 10;
		int total = sum(n);
		System.out.println("1 ~ " + n + "까지의 합 : " + total);
	}

	public static int sum(int n) {
		int sum = 0;
		int sum1 = 0;
		for (int i = 0; i <= n; i++) {
			sum = i;
			sum1 += sum;
		}
		return sum1;
	}
}
