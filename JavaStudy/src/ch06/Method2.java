package ch06;

public class Method2 {

	public static void main(String[] args) {
		int total = sum1(5, 4);
		System.out.println("합 메소드 실행결과 : " + total);
		double avg = 0;
		avg = total / 2;
		System.out.println("평균값 : " + avg);
	}

	public static int sum1(int num1, int num2) {
		int sum = 0;
		sum = num1 + num2;

		// System.out.println("sum : " + sum);
		return sum;
	}

}
