package ch06;

public class Exam6_4 {

	public static void main(String[] args) {
		int[] nums = { 1, 2, 3, 4 };

		int max = getMaxValue(nums);
		System.out.println("max : " + max);

		int min = getMinValue(nums);
		System.out.println("min : " + min);
	}

	public static int getMaxValue(int[] nums) {
		int max = Integer.MIN_VALUE;
		for (int i = 0; i < nums.length; i++) {
			if (nums[i] > max) {
				max = nums[i];

			}

		}
		return max;

	}

	public static int getMinValue(int[] nums) {
		int min = Integer.MAX_VALUE;
		// int min = num[0] 상수래요.
		for (int i = 0; i < nums.length; i++) {
			if (nums[i] < min) {
				min = nums[i];
			}

		}
		return min;

	}

}
