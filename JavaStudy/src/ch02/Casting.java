package ch02;

public class Casting {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		float sum = 0.0f;
		sum = 1.5f + 1.6f;
		System.out.println(sum);
		int transSum = (int) sum; // 형변환 ( 실수 -> 정수)
		System.out.println(transSum);

		char a = '1'; // 원래 a는 49.. 아스키코드입니다.
		System.out.println(a); // 그냥 찍음
		System.out.println((int) a); // 변환하면서 찍음(정수로변환)^^

		int b = 1000;
		byte b2 = 0; // 오버플로우 128-> - 127 , 1000 -> 24
		b2 = (byte) b; // 강제변환 20억 int -> long 추세..^^ or float, Double
		System.out.println(b2);

		boolean t = false; // 예약어 ㅋㅋㅋ
		boolean c = true;
		System.out.println("문자열" + t); //

	}

}
