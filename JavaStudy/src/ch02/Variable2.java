package ch02;

public class Variable2 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// 정수이므로 소수는 안됨.
		int age;
		age = 1; //
		System.out.println(age + 1);

		// 실수 쓸때는 1.2f
		float score = 1.2f;
		System.out.println(score);

		// 문자 (1개) 두글자는 안됨
		char ch = 'A';

		// 문자열 (여러개) - 한개도 가능..^^
		String str = "문자여러개";
		String str2 = "일"; // 문자열이지만 1개 가능

		System.out.println(age + score + ch + str + str2);
		// A = 숫자로 변환되면 65... 더하기 연산하면 숫자. (str, str2는 문자열과 합쳐짐)
		System.out.println(str2 + age + score + ch + str);
		// 다 문자열로 되어서 .... 결과가 달라

	}

}
