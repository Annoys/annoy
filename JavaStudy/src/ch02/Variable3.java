package ch02;

import java.awt.Color;

public class Variable3 extends Exam2 {

	private static final boolean True = false;

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		final char MALE = 'M'; // 상수는 대문자로 하자
		final char FEMALE = 'F';

		char a = 'M';
		char b = 'F';
		char gender = 'M'; // 성별

		gender = MALE; // gender = m (ctrl + space 정보제공)

		int abc$; // 이건됨!!
		int abb1; // int 1abb; ( 숫자로 시작되서 안됨)

		// 1970년도 1월 1일부터 시작된 시간. unixtime 2038년 20억숫자를 벗어남.. (-20억으로 돌아감)
		System.out.println("unix Time == " + System.currentTimeMillis());
		System.out.println("unix Time == " + System.nanoTime());

	}

}
