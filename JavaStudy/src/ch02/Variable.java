package ch02;

public class Variable {
	public static void main(String[] args) {
		System.out.println(1);
		// TODO Auto-generated method stub

		// 숫자(정수) - 소스 수정시 수정부분이 적음
		int age; // <- 선언, 선언만 하면 못쓴다.
		age = 10; // <- 초기화 공간을 가지게 된다.

		int grade = 4;
		System.out.println(age);
		System.out.println(age + grade);

		// age : 10 , grade : 4
		System.out.println("age : " + age + ", grade : " + grade);

		// age + grade = 14 문자열 + 문자열 + ( 숫자 + 숫자 ) -> 먼저연산
		System.out.println("age + grade = " + (age + grade));

	}

}
