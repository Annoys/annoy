package ch03;

public class Exam3_5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int num1 = 0, num2 = 0, num3 = 0, num4 = 0;
		int num5 = 0, num6 = 0, num7 = 0, num8 = 0;

		int result1 = (num1++) + (num2++); // num1++ 라인을 넘어가면 연산 그전에는 연산 x
		int result2 = (++num3) + (num4++); // ++num1 라인을 어가기전에 연산o
		int result3 = (num5++) + (++num6); // num1~8은 전부다 1값을 가진다.
		int result4 = (++num7) + (++num8);

		System.out.println(result1); // 0
		System.out.println(result2); // 1
		System.out.println(result3); // 1
		System.out.println(result4); // 2

	}

}
