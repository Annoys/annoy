package ch03;

public class Operatot3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// &&(n percent) &(and) | (or) ||(pipe line)
		int num1 = 0;
		int num2 = 0;
		boolean result = false;
		// & 한개는 무조건 실행 (조건1이 거짓이지만 뒤에 것도 보는거임..)
		result = num1++ < 0 & num2++ < 0;

		// && 두개는 상황에 따라 실행 (조건1이 거짓이므로 뒤에 조건은 보지도 않음)
		// Short-Circuit Evaluation (짧은순회연산..?)
		result = num1++ < 0 && num2++ < 0;

		System.out.println(result);
		System.out.println("num1의 값은" + num1 + ", num2의 값은" + num2);

		int num3 = 0;
		int num4 = 0;

		// || 앞이 참이면 뒤에 볼 필요가 없음 ㅇㅈ?
		result = ++num3 > 0 || ++num4 > 0;
		System.out.println("num3의 값은" + num3 + ", num4의 값은" + num4);
		// |
		result = ++num3 > 0 || ++num4 > 0;
		System.out.println("num3의 값은" + num3 + ", num4의 값은" + num4);

	}

}
