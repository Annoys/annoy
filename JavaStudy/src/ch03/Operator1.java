package ch03;

public class Operator1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int count = 0;
		count = count + 1; // 누적..
		count += 1;
		count = count / 2;
		count /= 2;

		count = count + 1;
		count++; // 증감연산자
		count--; // 1만큼 증가 감소

		System.out.println(2 != 3); // != 부정
		System.out.println(20 >= 3);

		System.out.println((count / 2 == 0) ? "T" : "F");

		// x > 0 그리고 y < 0

		int i = 0;
		int j = 0;
		++i;
		j = i;
		System.out.println(j); // = 0
		System.out.println(i); // = 1

	}

}
