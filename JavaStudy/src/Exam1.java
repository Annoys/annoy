
/**
 * 
 * @author : hyeonwoo 작성일자 : 2016-09-01
 *
 */
public class Exam1 {

	public static void main(String[] args) {
		// 1 ~ 3 연산결과출력
		// TODO Auto-generated method stub
		System.out.println("1 + 2 + 3 의 연산 결과는'" + (1 + 2 + 3) + "'입니다.");
		System.out.println("1 + 2 + 3 의 연산 결과는\"" + (1 + 2 + 3) + "\"입니다.");
		System.out.println("1 ~ 5 까지의 곱셈 결과는" + (1 * 2 * 3 * 4 * 5) + "입니다.");

		// 주석 1번 방식 - 힌줄주석
		/* 주석 2번 방식 - 여러줄 주석 */
		/** 주석 3번 방식 **/

	}

}