package ch07;

public class PersonMain {

	public static void main(String[] args) {

		Person p1 = new Person();
		p1.name = "김덕호";
		p1.height = 173.3f;
		p1.weight = 64.0f;

		Person p2 = new Person();
		p2.name = "양준호";
		p2.height = 176.6f;
		p2.weight = 77.3f;

		// p1.move(50);
		// p2.move(100);

		Person[] persons = new Person[2];
		persons[0] = p1;
		persons[1] = p2;

		for (int i = 0; i < persons.length; i++) {
			persons[i].move((1 + i) * 30);
		}

	}

}
