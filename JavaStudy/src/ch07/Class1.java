package ch07;

public class Class1 {
	int age;

	public static void main(String[] args) {
		// instance
		Class1 c = new Class1();
		c.age = -9;
		
	}

	public void setAge(int a) {
		if( a > 0  && a < 101) {
			age = a;  //캡슐화	
		} else {
			System.out.println("잘못된 나이입니다. 다시 입력해주세요");
		}
 		
	}
}
