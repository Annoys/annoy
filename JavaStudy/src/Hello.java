
public class Hello {

	public static void main(String[] args) {
		// 주석 comment, annoitation
		// TODO Auto-generated method stub
		System.out.print("HI~");

		// 큰따옴표를 사용하면 문자열을 표현
		System.out.println("안녕");

		// 숫자 출력
		System.out.println(1234);

		// 숫자 더하기
		System.out.println(1 + 1);

		// 문자 출력
		System.out.println('글');

		// 문자열 + 숫자 = 문자열로됨
		System.out.println("안녕" + 123);

		// 문자열 + 문자열
		System.out.println("문자열 +" + "문자열");

		// 1 + ( 1 * 5 )
		System.out.println("결과 : " + 1 + 2);
		System.out.println("결과 : " + (1 + 2));

		// 안녕하세요 저는 "조현우" 입니다. \" \"
		System.out.println("안녕하세요 저는 \"조현우\" 입니다.");

		// A의 점수 합은 3, B의 점수의 합은 5
		//
		System.out.println("A의 점수의 합은 " + (1 + 2) + ", B의 점수 합은 " + (2 + 3));

		int gradeOfClass;

	}

}
