package ch05;

public class Array1 {

	public static void main(String[] args) {

		int[] arr1 = new int[3];
		arr1[0] = 30;
		arr1[1] = 20;

		for (int i = 0; i < 3; i++) {
			System.out.println(arr1[i]);

		}

		int[] arr2 = { 60, 50, 40 };
		// System.out.println(arr1[5]);
		// 일반적인 for형태
//		for (int i = 0; i < arr2.length; i++) {
//			System.out.println(arr2[i]);

			// for - each
			for (int item : arr2) {
				System.out.println(item);
				
				
				
				

			}
		}
	}


