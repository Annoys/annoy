package ch05;

import java.util.Scanner;

public class Exam5_7 {

	public static void main(String[] args) {
		System.out.println("생성할 빙고판의 크기를 지정해주세요. ex) 5");
		System.out.print("크기 입력 : ");

		Scanner scan = new Scanner(System.in);
		int size = scan.nextInt();
		// 빙고판 생성
		int[][] numbers = new int[size][size];

		// 빙고판 값 입력 1 ~ size * size 만큼
		int number = 0;
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				number++;
				numbers[i][j] = number;
			}
		}
		// 빙고판의 숫자 섞기
		for (int i = 0; i < 1000; i++) {
			int row = (int) (Math.random() * size); // 0 ~ (size - 1)
			int col = (int) (Math.random() * size); // 0 ~ (size - 1)
			/* 랜덤 좌표의 값과 (0,0)의 값 바꾸기 */
			int temp = numbers[0][0];
			numbers[0][0] = numbers[row][col];
			numbers[row][col] = temp;
		}
		// 숫자섞기 결과 출력
		for (int i = 0; i < numbers.length; i++) {
			for (int j = 0; j < numbers[i].length; j++) {
				System.out.print(numbers[i][j] + "\t");
			}
			System.out.println();
		}

		scan.close();
	}
}