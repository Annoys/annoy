package ch05;

public class Exam5_1_3 {

	public static void main(String[] args) {

		int max = Integer.MIN_VALUE;
		int[] arr = { 1, 5, 11, 4, 11, 17 };

		for (int i = 0; i < arr.length; i++) {

			if (arr[i] > max) {
				max = arr[i];
			}

		}
		System.out.println(max);

	}

}
