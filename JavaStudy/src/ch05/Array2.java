package ch05;

public class Array2 {

	public static void main(String[] args) {

		int[] nums = new int[100];
		for (int i = 1; i <= 100; i++) {
			nums[i - 1] = i;

		}
		int sum = 0;
		for (int i = 0; i < 100; i++) {
			if (nums[i] % 2 == 0) {
				sum += nums[i];
			}

		}
		System.out.println(sum);

	}

}
