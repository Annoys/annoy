package Ch09;

// Launcher 런쳐.. 실행기^^
public class CarMain {

	public static void main(String[] args) {

		Car c = new Car();
		Truck tc = new Truck();
		SportsCar sc = new SportsCar();

		Car[] cars = { c, tc, sc };

		Truck t = (Truck) cars[1]; // 형변환...

		CarMain cm = new CarMain();
		cm.execute(tc); // 부모의형태로 tc -> c Up Casting
		cm.execute(sc); // 자식의형태로


	}

	// 넘겨받은 객체 실행
	public void execute(Car c)

	{
		c.move();
		c.openDoor();
		// Down Casting
		Truck tt = (Truck) c;
		tt.load();
//		tt.move();
		// c.load(); 자식이 갖고있는 메소드 실행하려면 형변환 해야댐
	}

}
